import { useRef, useState } from "react";
import { Form, Button, Card } from "react-bootstrap";

function Search() {
	const hasilInput = useRef(null)

	const [id, setId] = useState()

	const handleSubmit = (e) => {
		e.preventDefault();

		setId(hasilInput.current.value)

		console.log(hasilInput.current.value)
	}
	return (
		<>
			<Form onSubmit={handleSubmit}>
			<Form.Group className="mb-3">
				<Form.Label>Search</Form.Label>
				<Form.Control
					ref={hasilInput}
					type="text"
					placeholder="Penelusuran"
					name="cari"
				/>
				</Form.Group>
				<Button type="submit" variant="primary">Cari</Button>
				<p/>
			</Form>
			<Card class="show">
             <Card.Body>
				<p>
					Hasil Pencarian : {id} 
				</p>
			</Card.Body>
			</Card>
		</>
	)
}


export default Search;
