import { Form, Button, Row, Col } from "react-bootstrap";
import React, { useState } from "react"
import Forms from "../components/form"
import ModalUpdate from "../components/setUpdate"

const PlayerNew = () => {
    const [title, setTitle] = useState('Create Player')
	const [email, setEmail] = useState();
	const [username, setUsername] = useState();
	const [password, setPassword] = useState();
    const [address, setAddress] = useState();

    const handleClick = (event) => {
        setTitle('Create Player')
        event.preventDefault();
		setEmail(event.target.playerEmail.value);
		setUsername(event.target.playerUsername.value);
		setPassword(event.target.playerPassword.value);
        setAddress(event.target.playerAddress.value);

		event.target.playerEmail.value = "";
		event.target.playerUsername.value = "";
		event.target.playerPassword.value = "";
        event.target.playerAddress.value = "";
	}

    const editPlayer = (username, password, email, address) => {
		setEmail(email);
		setUsername(username);
		setPassword(password);
        setAddress(address);
    }
    return (
		<>
			<Row>
				<Col>
					<Form onSubmit={handleClick} method="post" action="#" align="left">
						<Forms
							label={"Email"}
							placeholder={"masukkan email Anda"}
							type={"email"}
							name={"playerEmail"}
						/>
						<Forms
							label={"Username"}
							placeholder={"masukkan username anda"}
							type={"text"}
							name={"playerUsername"}
						/>
						<Forms
							label={"Password"}
							placeholder={"masukkan password anda"}
							type={"password"}
							name={"playerPassword"}
						/>
                        <Forms
                        label={"Address"}
							placeholder={"masukkan alamat anda"}
							type={"address"}
							name={"playerAddress"}
                        />
						<p align="center">
						<Button variant="success" type="submit">
							Create New
						</Button>
						</p>
					</Form>
				</Col>
				<Col>
				<div class="card">
  					<div class="card-body">
						<p>{title}</p>
						<div id="tampilData" align="left">
							<p>Email : {email}</p>
							<p>Username : {username}</p>
							<p>Password : {password}</p>
                            <p>Address : {address}</p>
							<p align="center">
								<ModalUpdate
									email={email}
									username={username}
									password={password}
                                    address={address}
									onClick={editPlayer}
								/>
							</p>
						</div>
					</div>
				</div>
				</Col>
			</Row>
		</>
	)
}

export default PlayerNew;