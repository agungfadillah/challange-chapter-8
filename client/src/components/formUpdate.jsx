import { Form, Button } from "react-bootstrap";

const FormCreate = () => {
	return (
		<Form>
			<Form.Group className="mb-3">
				<Form.Label>Email</Form.Label>
				<Form.Control type="email" placeholder="email" />
			</Form.Group>
			<Form.Group className="mb-3">
				<Form.Label>Username</Form.Label>
				<Form.Control type="username" placeholder="username" />
			</Form.Group>
			<Form.Group className="mb-3">
				<Form.Label>Password</Form.Label>
				<Form.Control type="password" placeholder="password" />
			</Form.Group>
            <Form.Group className="mb-3">
				<Form.Label>Address</Form.Label>
				<Form.Control type="password" placeholder="address" />
			</Form.Group>
			<Button variant="primary" type="submit">
				Submit
			</Button>
		</Form>
	)
}

export default FormCreate