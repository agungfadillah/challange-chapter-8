import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';
import NewPlayer from "./components/NewPlayer";
import { Col, Container, Row, Button } from "react-bootstrap";
import Search from "./components/searchPlayer";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Container className="p-4">
			    <Row sm={2} md={1}>
				    <Col className="p-3">
					    <h1 align="center">Daftar Player</h1>
						<br></br>
					    <NewPlayer />
				    </Col>
				    <Col>
            			<Search align="center">Search</Search>
				    </Col>
			    </Row>
		    </Container>
      </header>
    </div>
  );
}

export default App;
